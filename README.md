# Market-Watch

Market watch is a text GUI bash script that pulls stock quote data from IEXCloud and displays it

## Installation

Clone the repository from gitlab

Validate curl and jq are installed
```bash
curl --version
jq --version
```
If not, install them
```bash
sudo apt install curl
#if using Debian:
sudo apt install jq
#If using snap:
snap install jq
```

## Usage

Set your IEXCloud secret as an environment variable and then run the script.
For every symbol you want to see, append them to the script call
```bash
export IEX_SECRET=value-here
./market-watch.sh "ba" "voo" "vti"
```


## License
[GPL3](https://choosealicense.com/licenses/gpl-3.0/)